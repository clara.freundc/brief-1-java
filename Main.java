// ArrayList pour ajouter des éléments à un tableau et utiliser des méthodes liées aux tableaux
import java.util.ArrayList;

// Création de la classe :
public class Main {
  public static void main(String[] args) {

    String[] employeeNames = {"Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly"};
    int[] hoursWorked = {35, 38, 35, 38, 40};
    double[] hourlyRates = {12.5, 15.0, 13.5, 14.5, 13.0};
    String[] positions = {"Caissier", "Projectionniste", "Caissier", "Manager", "Caissier"};
    String searchPosition = "Manager";

    // On initialise et déclare un tableau pour stocker les résultats de la recherche
    ArrayList<String> results = new ArrayList<String>();

    // Boucle for pour le calcul des salaires
    for (int i=0; i < employeeNames.length; i++) {
        if (hoursWorked[i] > 35) {
            System.out.println(((hoursWorked[i] - 35) * (1.5 * hourlyRates[i])) + 35 * hourlyRates[i]);
        } else {
            System.out.println(hoursWorked[i] * hourlyRates[i]);
        }   
    }

    // Boucle pour vérifier qu'il y ait l'employé recherché
    for (int i=0; i < employeeNames.length; i++) {
        if (searchPosition.equals(positions[i])) {
            results.add(employeeNames[i]);
        }
    }
    if (!results.isEmpty()) {
        System.out.println(results);
    } else {
        System.out.println("Aucun employé trouvé.");
    }
  }
}